﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    interface ISoundCloud<T>
    {
        List<T> GetPlaylists(int userId);
        List<T> GetPlaylists(string playlistNamePart);
        List<T> GetAlbums(int userId);
        List<T> GetAlbums(string albumNamePart);
        void AddSoundToPlaylist(int userId, int playlistId, File file);
        void DeletePlaylist(int userId, int playlistId);
    }
}
