import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/User';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userIdentityUrl = 'http://localhost:57620/users/';

  constructor(private http: HttpClient) { }

  getUserIdentity(): Observable<User> {
    return this.http.get<User>(this.userIdentityUrl);
  }

}
