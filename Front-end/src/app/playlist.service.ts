import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Playlist } from '../models/Playlist';

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {

  private playlistsUrl = 'http://localhost:57620/users/';

  constructor(private http: HttpClient) {
  }
  getPlaylists(username): Observable<Playlist[]> {
    return this.http.get<Playlist[]>(this.playlistsUrl + username + '/playlists');
  }
}
