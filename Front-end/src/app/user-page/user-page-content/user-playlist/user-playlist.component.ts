import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Playlist } from '../../../../models/Playlist';
import {DeleteService} from '../../../delete.service';

@Component({
  selector: 'app-user-playlist',
  templateUrl: './user-playlist.component.html',
  styleUrls: ['./user-playlist.component.css']
})
export class UserPlaylistComponent implements OnInit {

  @Input()playlist: Playlist;
  url: string;
  @Output() deletePlaylist: EventEmitter<any> = new EventEmitter();

  constructor(private deleteService: DeleteService) {
    this.url = 'http://localhost:57620/playlists/' + this.playlist.id + '/photo';
  }

  ngOnInit() {
  }

  onClickDelete() {
    this.deleteService.deletePlaylist(this.playlist.id);
    this.deletePlaylist.emit(this.playlist);
  }

}
