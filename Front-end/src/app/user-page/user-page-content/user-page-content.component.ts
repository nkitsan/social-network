import {Component, Input, OnInit} from '@angular/core';
import { Playlist } from '../../../models/Playlist';
import { Song } from '../../../models/Song';
import { Message } from '../../../models/Message';
import { Log } from '../../../models/Log';
import {User} from '../../../models/User';
import {UserService} from '../../user.service';
import {PlaylistService} from '../../playlist.service';
import {MessagesService} from '../../messages.service';
import {LogService} from '../../log.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-user-page-content',
  templateUrl: './user-page-content.component.html',
  styleUrls: ['./user-page-content.component.css']
})
export class UserPageContentComponent implements OnInit {

  identityUser: User;
  selectedUser: string;

  songs: Song[];
  playlists: Playlist[];
  albums: Playlist[];
  messages: Message[];
  logs: Log[];

  private allPlaylists: Playlist[];

  constructor(private route: ActivatedRoute,
              private userService: UserService,
              private playlistService: PlaylistService,
              private messageService: MessagesService,
              private logService: LogService) {
    this.songs = [];
    this.playlists = [];
    this.albums = [];
    this.messages = [];
    this.logs = [];
  }

  ngOnInit() {
    this.selectedUser = this.route.snapshot.paramMap.get('username');
    this.userService.getUserIdentity().subscribe((user: User) => this.identityUser = user);
    const currentUser = this.selectedUser || this.identityUser.nickname;
    this.playlistService.getPlaylists(currentUser).subscribe(playlists => this.allPlaylists = playlists);
    for (const playlist of this.allPlaylists) {
      if (playlist.isOfficial) {
        this.albums.push(playlist);
      } else {
        this.playlists.push(playlist);
      }
    }
    if (!this.selectedUser) {
      this.messageService.getMessages(this.identityUser.nickname).subscribe(messages => this.messages = messages);
      this.logService.getLogs(this.identityUser).subscribe(logs => this.logs = logs);
    }
  }

  deleteSong(id: any) {
    for (let i = 0; i < this.songs.length; i++) {
      if (this.songs[i].id === id) {
        delete this.songs[i];
        break;
      }
    }
  }

  deletePlaylist(playlist: Playlist) {
    if (playlist.isOfficial) {
      for (let i = 0; i < this.albums.length; i++) {
        if (this.albums[i].id === playlist.id) {
          delete this.albums[i];
          break;
        }
      }
    } else {
      for (let i = 0; i < this.playlists.length; i++) {
        if (this.playlists[i].id === playlist.id) {
          delete this.playlists[i];
          break;
        }
      }
    }
  }
}
