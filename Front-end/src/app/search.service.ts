import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Song } from '../models/Song';
import { Playlist } from '../models/Playlist';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private songsUrl = 'http://localhost:57620/search/songs?=';
  private playlistsUrl = 'http://localhost:57620/search/playlists?=';
  private usersUrl = 'http://localhost:57620/search/user?=';

  constructor(private http: HttpClient) {
  }

  getSongs(startsWith): Observable<Song[]> {
    return this.http.get<Song[]>(this.songsUrl + startsWith);
  }

  getPlaylists(startsWith): Observable<Playlist[]> {
    return this.http.get<Playlist[]>(this.playlistsUrl + startsWith);
  }

  getUsers(startsWith): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl + startsWith);
  }
}
