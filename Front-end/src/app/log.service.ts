import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Log } from '../models/Log';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  private logsUrl = 'http://localhost:57620/logs/';

  constructor(private http: HttpClient) {
  }

  getLogs(username): Observable<Log[]> {
    return this.http.get<Log[]>(this.logsUrl + username);
  }
}
