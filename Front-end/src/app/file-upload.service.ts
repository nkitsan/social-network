import { Injectable } from '@angular/core';
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  private urlImg = 'http://localhost:57620/users/';

  constructor(private http: HttpClient) { }

  postFileImg(fileToUpload: File, username: string): Observable<boolean> {
    const formData: FormData = new FormData();
    formData.append('file', fileToUpload);
    this.http.post(this.urlImg + username + '/photo', formData);
    return of(true);
  }
}
