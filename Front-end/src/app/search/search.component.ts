import {Component, Input, OnInit} from '@angular/core';
import {Playlist} from '../../models/Playlist';
import {User} from '../../models/User';
import {Song} from '../../models/Song';
import {SearchService} from '../search.service';
import {ActivatedRoute} from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  match: string;
  counter: number;

  playlists: Playlist[];
  songs: Song[];
  users: User[];

  constructor(private route: ActivatedRoute, private location: Location, private searchService: SearchService) { }

  ngOnInit() {
    this.counter = 3;
    this.match = this.route.snapshot.paramMap.get('part');
    this.searchService.getPlaylists(this.match).subscribe(playlists => this.playlists = playlists);
    this.searchService.getSongs(this.match).subscribe(songs => this.songs = songs);
    this.searchService.getUsers(this.match).subscribe(users => this.users = users);
  }

  onClickPlaylist() {

  }

  onClickMorePlaylists() {
    this.songs = [];
    this.users = [];
    this.counter = this.playlists.length;
  }

  onClickMoreSongs() {
    this.users = [];
    this.playlists = [];
    this.counter = this.songs.length;
  }

  onClickMoreUsers() {
    this.songs = [];
    this.playlists = [];
    this.counter = this.songs.length;
  }

  goBack(): void {
    this.location.back();
  }
}
