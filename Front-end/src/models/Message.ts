export class  Message {
  time: string;
  fromUser: string;
  toUser: string;
  text: string;
  soundId: number;
  soundName: string;
  soundAuthor: string;
}
