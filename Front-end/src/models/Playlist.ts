export class Playlist {
  name: string;
  id: number;
  isOfficial: boolean;
}
