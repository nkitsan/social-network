export class Song {
  name: string;
  author: string;
  id: number;
}
