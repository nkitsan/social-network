﻿using System.ComponentModel.DataAnnotations;

namespace Backend.ViewModels
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Username not defined")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Email not defined")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Password not defined")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Password is incorrect")]
        public string ConfirmPassword { get; set; }
    }
}