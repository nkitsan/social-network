﻿using System.ComponentModel.DataAnnotations;

namespace Backend.ViewModels
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Username not defined")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password not defined")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}