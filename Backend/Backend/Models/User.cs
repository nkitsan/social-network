﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class User
    {
        public string Username { get; set; }

        public bool IsAdmin
        {
            get
            {
                return DBWork.Instance.GetUser(Username).Value.Role == "ADMIN";
            }
            set
            {
                
            }
        }
    }
}
