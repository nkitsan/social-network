﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class Sound
    {
        public int SoundId;
        public string Name;
        public string Author;
    }
}
