﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Backend.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Backend.Controllers
{
    [Produces("application/json")]
    [Route("api/search")]
    public class SearchController : Controller
    {
        //в каждом проверить, начинается ли с этого строка

        [Authorize]
        [HttpGet("{id}/songs")]
        public async Task<IActionResult> GetSongs([FromQuery] string search)
        {
            return Ok(DBWork.Instance.GetSongsByPrefix(search).Select(x => new Sound { Author = x.Author, Name = x.Name, SoundId = x.ID }));
        }

        [Authorize]
        [HttpGet("{id}/playlists")]
        public async Task<IActionResult> GetPlaylists([FromQuery] string search)
        {
            return Ok(DBWork.Instance.GetPlaylistsByPrefix(search).Select(x => new SoundCollection { SoundCollectionId = x.ID, Name = x.Name, IsOfficial = x.IsOfficial }));
        }

        [Authorize]
        [HttpGet("{id}/users")]
        public async Task<IActionResult> GetUsers([FromQuery] string search)
        {
            return Ok(DBWork.Instance.GetUsersByPrefix(search).Select(x => new User { Username = x.Username }));
        }
    }
}