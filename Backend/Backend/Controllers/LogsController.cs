﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Backend.Models;

namespace Backend.Controllers
{
    [Produces("application/json")]
    [Route("api/logs")]
    public class LogsController : Controller
    {
        [Authorize]
        [HttpGet("{username}")]
        public async Task<IActionResult> Get(string username)
        {
            List<Log> logs = new List<Log>();
            if (DBWork.Instance.GetUser(username)?.Role == "ADMIN")
                logs = DBWork.Instance.GetLogs().Select(x => new Log { Message = x.Message, UserId = x.UserID, Time = x.Time }).ToList();
            return Ok(logs);
        }
    }
}